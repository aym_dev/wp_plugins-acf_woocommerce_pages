# ACF WooCommerce Pages

Adds WooCommerce linked pages

## Installation

### Using Composer

Below the minimum composer file you'll need in your project theme
You could of course go with nicer solutions and not put this directly in
your theme

	{
		"name":"My project",
		"require": {
			"aym_dev/wp_plugins-acf_woocommerce_pages": "dev-master"
		},
		"repositories": [
			{
				"type": "git",
				"url": "git@bitbucket.org:aym_dev/wp_plugins-acf_woocommerce_pages.git"
			}
		],
		"extra": {
			"installer-paths": {
				"../../mu-plugins/{$name}/": ["type:wordpress-muplugin"],
				"../../plugins/{$name}/": ["type:wordpress-plugin"],
				"../{$name}/": ["type:wordpress-theme"]
			}
		}
	}

### Direct download

Just put this entire directory inside your plugins folder

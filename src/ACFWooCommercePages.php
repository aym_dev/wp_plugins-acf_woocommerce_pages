<?php
namespace AllYourMedia\ACFWooCommercePages;
use acf_location;
/**
 * Can't extend the original so this adds another section
 * Which is probably nicer anyway
 *
 * Removed the original comments, they should be read from the original ACF file
 */
class ACFWooCommercePages extends acf_location {

	public function initialize()
	{
		$this->name = 'page_type_woocommerce';
		$this->label = __("Page WooCommerce", 'acf_woocommerce_pages');
		$this->category = 'page';
	}

	public function rule_match($result, $rule, $screen)
	{
		$post_id = (int) acf_maybe_get($screen, 'post_id');

		// bail early if there is no post id
		if (!$post_id) {
			return false;
		}

		if (
			'pay' === $rule['value'] ||
			'thanks' === $rule['value']
		) {
			// Doesn't exist since 2.1
			// See woocommence/includes/wc-page-functions.php
		} else if (
			'change_password' === $rule['value'] ||
			'edit_address' === $rule['value'] ||
			'lost_password' === $rule['value']
		) {
			// Doesn't exist since 2.1 either
		} else {
			// All other pages - check same list as displayed
			foreach ($this->rule_values() as $key => $label) {
				if ($key === 'any') {
					continue;
				}
				if ($rule['value'] === $key || $rule['value'] === 'any') {
					// todo verify getting page ID with wpml
					// But, we use wc_get_page_id and if that doesn't work
					// the whole woocommerce plugin should break anyway
					$result = $post_id === wc_get_page_id($key);

					if ($result) {
						break;
					}
				}
			}
		}

		// reverse if 'not equal to'
		if ($rule['operator'] === '!=') {
			$result = !$result;
		}

		// return
		return $result;
	}

	// Return the WooCommerce pages that can linked to an actual page
	// Added defaults to the args so this function can be re-used above
	public function rule_values(array $choices = [], $rule = null)
	{
		return [
			'any'	=> __("Any", 'acf_woocommerce_pages'),
			'shop'	=> __("Shop", 'acf_woocommerce_pages'),
			'cart' => __("Cart", 'acf_woocommerce_pages'),
			'checkout' => __("Checkout", 'acf_woocommerce_pages'),
			// 'pay' => __("Pay", 'acf_woocommerce_pages'),
			// 'thanks' => __("Thanks", 'acf_woocommerce_pages'),
			'myaccount' => __("My account", 'acf_woocommerce_pages'),
			// 'edit_address' => __("Edit address", 'acf_woocommerce_pages'),
			'view_order' => __("View orders", 'acf_woocommerce_pages'),
			'terms' => __("Terms", 'acf_woocommerce_pages'),
		];
	}

}

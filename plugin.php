<?php
/*
Plugin Name: ACF WooCommenrce Pages
Plugin URI: https://www.allyourmedia.nl
Description: Adds WooCommerce linked pages
Version: Master
Author: Allyourmedia
Author URI: https://www.allyourmedia.nl
Copyright: Allyourmedia
*/

add_action('init', function($version) {
	if (function_exists('acf_register_location_rule')) {
		load_textdomain(
			'acf_woocommerce_pages',
			__DIR__ . '/languages/' . get_locale() . '.mo'
		);

		require 'src/ACFWooCommercePages.php';
		acf_register_location_rule(
			'AllYourMedia\ACFWooCommercePages\ACFWooCommercePages'
		);
	} else {
		// Maybe show some message you actually need ACF
	}
});
